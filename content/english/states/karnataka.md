+++
bg_image = "/images/73af161a-ee3c-4449-9b04-7379f111671f-height-500.jpg"
date = 2021-10-30T18:30:00Z
description = ""
image = "/images/73af161a-ee3c-4449-9b04-7379f111671f-height-500.jpg"
location = "Bengaluru"
members = []
state_website_url = ""
title = "Karnataka"

+++

India Labour Line a National level legal mediation helpline project took off in Bengaluru in July 2021.

The core team member of the centre has received immersive training on handling the legal dispute of informal workers in Udaipur and Bengaluru.

Since its inception, the Bengaluru centre of India Labour Line has registered more than 40 cases and has been able to solve a wage dispute of about 4laks so far.

The centre collaborated with many unions, networks, and civil societies for outreach purposes. The centre has reached out to the workers for its promotion through various activities including speaking on FM radio, wall paintings, distribution of handbills & Visiting cards, etc.

The centre is also in the process of building a cadre of volunteers who will help out in outreach activity and legal support for the Labour Line project. So far about 12 volunteers have been identified for the same.

Karmika Gaurav Kendra is a membership-based worker facilitation centre that was started in Bengaluru in October 2020. The main objective of the centre is to provide coverage to all the informal workers (94%) in the city. It includes registration, linkages with the govt scheme, collectives, and a platform to get work opportunities for workers in Bengaluru.

In the last 10 months, we have been able to map more than 60 informal clusters in and around Bannerghatta Road of Bengaluru. Out of 60 clusters, we are engaging with more than 30 clusters of it for our various purposes as highlighted in the first point.

So far more than 1000 workers who are primarily working in the construction sector have been registered under the centre. The workers who are registered will be provided with an identity card from the centre on the annual fee of 100 rupees that directly goes to the worker fund. The process of opening a bank account with the signatories of workers’ representatives is in the process and will be finalized by this month.

Other than centre-based registration we have also been able to register about 92 workers under BOCW who have been provided with the Labour card. Also, about 40 workers who were registered under BOCW have received cash support that was announced by the Karnataka govt recently in the wake of the second wave of covid19. Also, 100 workers have been provided with the ration kit through BOCW boards to support them during the lockdown.

The centre has also helped in vaccinating about 100 registered workers with the support of the Godrej foundation that offered the support. The second dose of the worker is due this month and will be provided very soon.

WPC Bengaluru chapter meeting was organized on the 9th of August in SCM house. The meeting was organized by WPC, Hasiru Dala, Grakoos, and Aajeevika bureau. The main theme of the meeting was to discuss the issue of the informal workers in Bengaluru. It was attended by more than 30 participants who work on the various issues of informal workers in the city.

In the meeting the main discussion was held on the topic such as on the registration of the informal workers, campaign for a Universal ESIC coverage for all informal workers, the need of rental housing for the informal workers, the issue of the elderly or retired informal workers, etc.