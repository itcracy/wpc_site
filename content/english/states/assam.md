+++
bg_image = "/images/blog/employee_mha_order.jpg"
date = 2021-10-30T18:30:00Z
description = ""
image = "/images/blog/employee_mha_order.jpg"
location = "Guwahati"
members = []
state_website_url = ""
title = "Assam "

+++
The first chapter meeting was organised in Assam on 27th September 2021

Chapter agreed to organise 2 webinars - one on wages and another on workers housing.

The discussion also planned to do outreach for potential groups and bring them part of these initial discussions of WPC.

Along with this, we are also starting state chapters in West Bengal, Jharkhand, Odisha, Chhattisgarh and Haryana.