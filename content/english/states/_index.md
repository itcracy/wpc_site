---
title: Associated States
bg_image: "/images/1129018.jpeg"
description: WPC is in the process of creating solidarities and a coalition between  all
  the labour organisations and trade unions which will form different state-level  chapters.
  The loose nature of the coalition, as opposed to a tighter regulated unions  federation,
  makes this body comfortable for all kinds of organizations working in  the informal
  sector to come together.
menu:
  main:
    name: State Chapters
    URL: states
    weight: 12

---
