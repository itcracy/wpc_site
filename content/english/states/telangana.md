+++
bg_image = "/images/139160-vekcyivyru-1585498637.jpeg"
date = 2021-10-30T18:30:00Z
description = ""
image = "/images/139160-vekcyivyru-1585498637.jpeg"
location = "Hyderabad"
members = []
state_website_url = ""
title = "Telangana"

+++
Telangana Chapter organised a solidarity meeting in Hyderabad in support of the farmers strike against the unfair and exploitative Farm bills on 19th August 2021 that had the participation of 43 labour union leaders, academics, activists and workers organisations.

The meeting was attended by Dr Gangadhar JCL who is the CEO of E-Shram registration in the state and Pr. Shantha Sinha, former Chairperson of NCPCR. It was decided to work intensively at a state-level platform taking up issues of unorganised workers that include state migrant workers policy, engagement with the State Welfare Board, registration of unorganised workers under the E-Shram.

The members participated in the solidarity program with farmers on 27th September. They declared that the struggle of farmers for the withdrawal of the Farm Bill is that of all workers in the country. It also declared that the four labour codes that threaten to take away the hard-earned historical rights of workers will be resisted.

The chapter made a representation to the government of Telangana on formation of Social Security Boards in line with Unorganised Social Security Act 2008. Based on this, the chapter presented the ongoing work of workers facilitation centre and helpline - India Labourline.

Also, the chapter made representation of E-Shram Cards and the rules of Social Security codes.

State Facilitation Centre of India Labourline (national helpline for mediation and legal aid for workers) was inaugurated by Pro. Shantha Sinha, former Chairperson of NCPCR on 19th August.

The state team consisting of Anji Reddy, Vasu and Frederick have been working relentlessly to bring awareness among Unorganised Workers both in communities and labour Addas. Till now, 92 Labour Addas have been covered in Hyderabad City.