+++
bg_image = "/images/blog/home_about.jpg"
date = 2021-10-30T18:30:00Z
description = ""
image = "/images/about/home_about.jpg"
location = "Lucknow"
members = []
state_website_url = ""
title = "Uttar Pradesh "

+++
In April 2021, because of the elections and as per the Supreme court’s mandate the UP government had implemented many schemes in terms of PDS and cash transfer (Rs 1000/-) through the panchayat, but the distribution was unequal and the whole process was so slow that still many people are not able to access it. The government was planning to use the money from BOCW in different ways.

• Therefore, the UP chapter was struggling to come up with a strategy to come together for joint action, but they are meeting and exploring many strategies to form demands and campaigns around it.

• AKAM conducted 2 small meetings to discuss the possibilities to put the demands in front of the CM.