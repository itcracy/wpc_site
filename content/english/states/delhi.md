+++
bg_image = "/images/58c0e40ee226450e87e6ec696b0378f7_18.jpeg"
date = 2021-10-29T18:30:00Z
description = ""
image = "/images/58c0e40ee226450e87e6ec696b0378f7_18.jpeg"
location = ""
members = []
state_website_url = ""
title = "Delhi"
type = ""

+++
* India Labour Line a National level legal mediation helpline project took off in Delhi in July 2021.


* The core team member of the centre has received immersive training on handling the legal dispute of informal workers in Udaipur.


* Advocacy with state govt on intensifying registration of informal workers


* Creating SOP for simplifying the registration process


* Consolidating efforts for further intervening in the supreme court on ‘migrant suo moto case’


* Building the agenda of workers housing