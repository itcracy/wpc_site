---
title: Sanchaar Interpreters Network
description: Language is a major obstacle to organization of national level networks
  of grassroots organisations. Sanchaar is being set up to facilitate communication
  between activists in various states who are divided by barriers of language.
bg_image: "/images/w.jpg"
related_tag: sanchaar_interpreters_network
image: "/images/sanchaar.jpg"
facts:
  enable: false
  fact_item: []
menu:
  main:
    URL: sanchaar_interpreters_network
    weight: 13
    parent: home

---
##### **Launching SANCHAAR: language interpreters network for Indian languages:**

![](/images/sanchaar.jpg)

The Working People’s Coalition (WPC) is calling for volunteer interpreters to become part of its interpreters' network, **SANCHAAR**, to help in bridging the communication gap between its various member organisations across India for its national-level online meetings, webinars, etc. Sanchaar partners with the International Alliance of Inhabitants and the other global volunteer networks like the groups in the World Social Forum.

The Working People's Coalition (WPC) is a network of organizations working on issues related to labour in general and informal labour in particular. It is an independent entity not affiliated with any organization, federation or political party. The WPC is open to all organizations working with informal labour – in organizing, support, research, training, skilling etc.- irrespective of affiliation, sector, or geography.

India is a land of almost a hundred languages. The question of communication across the barrier of language is a serious problem. WPC’s membership stretches across the country, encompassing people who speak different languages. Most of those who work in these organisations speak primarily, and often only, in their native language, and therefore the issue of communication is extremely critical. SANCHAAR is useful for outreach meetings and meetings within global forums as well, making it possible for Indian organisations to participate more actively in global meetings, where SANCHAAR can provide interpretation in Indian languages.

**WPC calls upon all those who can interpret from one Indian language to another and also between English and Indian languages to join us on this exciting and inspiring journey for social change and the fight for justice.** The work will be on a volunteer basis for now, and we hope that it will be an opportunity for gaining experience, even for a professional interpreter. Beginners, amateurs and professionals are all welcome to register with us. You will be called upon for webinars, national-level meetings and for smaller group discussions that are multi-lingual.

**Please get in touch to volunteer with us and become part of the SANCHAAR network.**

Those who can interpret only one way i.e from language A to language B but not B to A are also welcome. SANCHAAR is mainly focused on Indian languages, those who can interpret in foreign languages too and from English and Indian languages are also encouraged to join since the global networks with which SANCHAAR is associated will be happy to welcome new languages for their work.

**Students of language institutes please feel free to register.**

**All interpreters including professional people are encouraged to join.**

We hope that we can provide an experience even if payment might not be possible at this time. For newer people who need training with the software, training will be provided free of cost.

**To join and to address your queries please email us with your details and bio-data.**

[Click here to apply](https://workingpeoplescharter.in/contact/)