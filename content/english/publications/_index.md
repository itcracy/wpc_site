---
title: Publications
bg_image: "/images/1129018.jpeg"
description: 'The Working Peoples'' Charter has been extensively involved in research
  and providing support to numerous groups and unions working with unorganized labour.
  Attached below are our recent publications. '
menu:
  main:
    URL: publications
    weight: 14

---
