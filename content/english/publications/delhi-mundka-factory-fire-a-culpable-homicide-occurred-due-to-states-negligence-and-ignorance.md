+++
bg_image = "/images/w.jpg"
date = 2022-07-04T08:30:00Z
description = "On May 13, 2022, a fire broke out in a four-storey building of Cofe Impex Pvt Ltd, a manufacturing factory of electronic and surveillance equipment located in Delhi’s Mundka industrial area. The incident occurred during a motivational lecture being attended by over a hundred workers - a majority of whom were women- and other company representatives."
image = "/images/mundka-fact-finding-report-design-1.png"
source_url = "https://drive.google.com/file/d/1ElF5geaJmdEJtfNWYrSD1gQmVzHJRmqV/view?usp=sharing"
tags = ["labour_axis", "india_labour_line"]
title = "Delhi Mundka Factory Fire: A Culpable Homicide Occurred Due to States Negligence and Ignorance "
[[publishers]]
designation = "State Chapter"
image = ""
name = "WPC Delhi Chapter"

+++
On May 13, 2022, a fire broke out in a four-storey building of Cofe Impex Pvt Ltd, a manufacturing factory of electronic and surveillance equipment located in Delhi’s Mundka industrial area. The incident occurred during a motivational lecture being attended by over a hundred workers - a majority of whom were women- and other company representatives. **According to official data, the fire resulted in the death of 27 workers, out of which 21 were women.** 

This was not surprising since nearly 90% of the workforce in this factory were women. Most were below 30, some as young as 18-19 years. At least 40 workers were gravely injured and many others were reported missing. Further, the incident has far-reaching administrative, legal and socio-economic consequences as well as gendered implications for the victims. A majority of the affected families have been forced to live in a state of severe financial hardship and emotional distress. Considering the gravity of this devastating incident, a group of experts including trade unionists, lawyers, women’s rights activists, and members of labour rights NGOs embarked on a fact-finding mission and conducted a detailed investigation into the causes of the incident and the long-term impact and irreversible consequences on the workers, especially migrant workers.

**Click here to download the full report:**

[Delhi Mundka Factory Fire: A Culpable Homicide Occurred Due to States Negligence and Ignorance](https://drive.google.com/file/d/1ElF5geaJmdEJtfNWYrSD1gQmVzHJRmqV/view?usp=sharing)