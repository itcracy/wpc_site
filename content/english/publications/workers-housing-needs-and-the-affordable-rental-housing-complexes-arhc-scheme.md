+++
bg_image = "/images/screenshot-2021-10-17-at-3-54-29-pm.png"
date = 2021-08-06T07:02:31Z
description = ""
image = "/images/screenshot-2021-10-17-at-3-54-29-pm.png"
source_url = "https://drive.google.com/file/d/1qJEWXkm0eB3ZLqYavL2uosvaPLw045kF/view?usp=sharing"
tags = ["labour_axis"]
title = "Workers Housing Needs and the Affordable Rental Housing Complexes (ARHC) Scheme"
type = ""
[[publishers]]
designation = ""
image = ""
name = "Shweta Damle "
[[publishers]]
designation = "Indian Institute for Human Settlements"
image = "/images/swastik-harish.jpeg"
name = "Swastik Harish"
[[publishers]]
designation = "Fellow, Centre for Policy Research "
image = "/images/mukta_naik-2.jpeg"
name = "Mukta Naik"

+++
The ARHC scheme that was announced by the Government of India as a part of the Rs 20 lakh crore Atmanirbhar Bharat Abhiyan relief package, envisages the creation of affordable rental housing for the migrants and the urban poor. The mode of delivery is based on involving private and public agencies for the upgradation and operation of the existing vacant housing units that were constructed in the past under other government schemes. It also encourages private and public institutions to construct workers housing on their own land through a concessionaire agreement and providing a viability gap fund.

###### **Meena Menon and Chandan Kumar**

###### **Working Peoples’ Charter (WPC) Network**

###### **Please download the full statement, ARHC report and Resolution here:**

###### [Statement - ARHC report launch](https://drive.google.com/file/d/14v4nLwfC9oeBiaKicbnHkJ7sAy-PJ4in/view?usp=sharing)

###### [ARHC Scheme report - 30072021](https://drive.google.com/file/d/1qJEWXkm0eB3ZLqYavL2uosvaPLw045kF/view?usp=sharing)

###### [Resolution - ARHC Scheme report launch](https://drive.google.com/file/d/1E4MSfYGv_5eG8M6A3OQ5zdI-M5lja2Gt/view?usp=sharing)