+++
bg_image = "/images/20211023095625_img_3211.JPG"
date = 2021-10-23T18:30:00Z
description = "The Working Peoples Charter (WPC) network a coalition of unorganized worker organizations working on issues related to informal labour organized a national conference at the Montfort Social Institute, Hyderabad. The conference saw the participation of more than 150+ informal labour activists from 23+ states and deliberated on the needed strategy to build a national movement for unorganized workers."
image = "/images/20211023095625_img_3211.JPG"
tags = []
title = "Activists from Unorganized Workers' Organizations show strength at their largest gathering since COVID-19 pandemic in Hyderabad"
type = ""

+++
#### **PRESS RELEASE**

#### _Activists from Unorganized Workers’ organizations show strength at their largest gathering since COVID-19 pandemic in Hyderabad_

_24th October 2021_

The Working Peoples Charter (WPC) network a coalition of unorganized worker organizations working on issues related to informal labour organized a national conference at the Montfort Social Institute, Hyderabad. The conference saw the participation of more than 150+ informal labour activists from 23+ states and deliberated on the needed strategy to build a national movement for unorganized workers.

Over the course of 2 days, delegations identified seven key areas of intervention. These include extending benefits through the e-Shram portal which are at par with benefits accruing to formal workers, implementing a universal social protection floor, extending Employees’ State Insurance Scheme without exclusionary standards, revising housing policies to accommodate the basic need for affordable and dignified workers’ housing, to strengthen and restore democratic processes on labour law reforms and labour administration.

“This is a historic conference. After COVID-19 hit, this is perhaps the largest congregation of informal workers’ organizations in India. Today we have received solidarity from Kashmir to Assam, from Gujarat to Tamil Nadu. WPC will soon take flight towards becoming a national platform to strengthen the interests” said Brother Varghese Thecknath of the Montfort Social Institute who were hosts for the congregation.

As Chief Guest at the opening, Sri. Devendra Reddy, Chairman of the newly constituted Telangana Unorganized Social Security Board expressed his dissatisfaction with the glacial pace with which the government was responding to informal sector’s concerns. He pointed out stating, “we have not been able to accomplish much. Even with the e-Shram portal now being implemented, we have to take it a step further and extend it to social security schemes. But I assure you our new social security board vows stand with the workers firmly.”

An informal worker is footloose, a migrant, pushed out of their homes and unwelcome at their destination. An informal worker is not a worker in the eyes of the law. An informal worker is everyone without labour rights. 9 out of every 10 workers in India are informal and largely unorganized into collectives. And when a calamity like COVID-19 hits the world, the country, the town, or the village, it is the informal workers who hold up the society and the economy – yet unpaid, yet unseen and yet unheard.

The Government of India legislated the Code on Social Security 2020 ‘with the goal to extend social security to all employees and workers either in the organised or unorganised or any other sectors’. As the rules under this law are being drafted, the delegates resolved that this exercise must also guarantee universality and equity of social protection to all workers. Such a guarantee can be effectively realized through a National Social Protection Floor constituted in line with the ILO’s Recommendation 202 (2012) as ‘nationally defined sets of basic social security guarantees which secure protection aimed at preventing or alleviating poverty, vulnerability and social exclusion’.

“On this day, the WPC stands as a collective voice of informal workers in this country who are demanding social justice and dignity. We will persevere with unflinching commitment to the ideals of this Sovereign Socialist Secular Democratic Republic. We are the united front of the historically marginalized but inevitable future of this country – the working people. The women, men, trans-people, adivasis, dalits and religious minorities in the WPC resolve in unison, to build a united collective based on the principles of equity and non-discrimination, and committed to our goals stated in our passed resolution” said Chandan Kumar the national coordinator for the Working Peoples’ Charter.

“This is an extremely important gathering. Working Peoples’ Charter is a very important socio-economic imitative. Working People refers to workforce in India and we are 500 million in India. It includes the working class but is not confined to it. India where capitalism is superimposed over a vast feudal base. Hence because of this multi structural society with probing questions of caste and (tribals), we need a broader alliance”, probed noted Professor and labour laws expert Babu Mathew while explaining the continuous attack labour laws have faced in recent years.

Rajiv Khandelwal from Aajeevika Bureau commented that “despite covid, what remains unaddressed is the migrants’ relationship to the city. State is not the only policy audience, and we must have research-based dialogue with the industry and there is a strong business case for it. We need more people and resources to extend support to struggling migrant workers on the ground.”

Paul Divakar from the National Campaign for Dalit Human Rights, “Structural discrimination exists, and workers face this every day. Because of peoples’ social locations, they suffer extreme oppression. ‘Working People’ as a term is a conscious choice and this charter of demands must move forward towards adequate implementation of our laws and processes.”

Gautam Bhan from the Indian Institute of Human Settlements, “I don’t think there are spaces left in this country which permit such diversity. WPC is one such space and I think what the last two years have taught us is to allow uncertainty to persist. We must experiment with new ways to fight for not just a minimum wage but a joyful wage.”

The Working President of the WPC, Meena Menon congratulated the delegations with the following comments, “on this day, the WPC stands as a collective voice of informal workers in this country who are demanding social justice and dignity. This two day conference will become a launching pad to bigger coalitions and broader goals. We must stand and fight for the working people and this is only the beginning and we will create build history”

Shaik Salauddin, General Secretary of the Indian Federation of App Based Transport Workers concluded the session by saying, “I salute the courage of every leader that is here today. WPC has enabled various smaller unions working arduously in the corners of the country to find a voice. I thank everyone for their solidarity and support.”

On behalf of Hyderabad Organising Committee

Bro Varghese T, Sister Lissy J., Shahi Kumar, Shaikh Salauddin

For more information, please contact:

Shaik (9177624678) Vivek (9538516282) Pratik (9920486727

The 150 delegates from 23 states in India, present at the annual National Conference of the Working People’s Charter (WPC) came together on 23rd-24th October 2021 in Hyderabad to raise the collective voice of such unorganized workers. We pass the resolution of demands of the unorganized workers of India that make the agenda of the WPC.

[Final WPC Resolution 2021 - PDF Version](https://drive.google.com/file/d/11BWV_2rkSktHv5aduzHN7HNs7hrzlQHg/view?usp=sharing "WPC National Conference 2021 Resolution")