+++
bg_image = "/images/nationallaunch.jpg"
date = 2021-09-17T18:30:00Z
description = "The Working Peoples Charter (WPC), along with Aajeevika Bureau, launched the National Helpline for workers on September 18, 2021, at the Press Club in Mumbai. The helpline, known as the India Labourline, has been operational since July 16, 2021, and has its headquarters in Mumbai. In the last two months, the helpline has received 2497 calls and provided mediation to 507 workers in distress from states like Delhi, Rajasthan, Uttar Pradesh, Haryana, Punjab, Jammu Kashmir, Madhya Pradesh, Odisha, Maharashtra, Karnataka, Kerala, Telangana and Tamil Nadu. It was started in response to the urgency to create a framework to address the issues and bridge the gap in providing legal aid and mediation services for the problems faced by workers at the worksite."
image = "/images/nationallaunch.jpg"
tags = ["india_labour_line"]
title = "NATIONAL LAUNCH OF INDIA LABOURLINE – A GLIMMER OF HOPE FOR INFORMAL WORKERS"
type = ""

+++
Aajeevika Bureau responds to a large volume of complaints from migrant workers who report wage denials and fraud, mistreatment, forced labour, unfair retrenchments or denial of due compensation. We rely heavily on counselling and mediation to resolve civil disputes, and also extend legal aid and counsel when necessary. Aajeevika’s interventions have been successful in recovering over 31 crores for unorganised and migrant workers – money that would have otherwise been lost in the absence of an accessible and proactive platform for registration and redressal of disputes.

Aajeevika launched a labour helpline, called the Labour Line in 2015. Labour Line is a phone-based helpline for workers operated in partnership with the Labour Department, Government of Rajasthan. The Labour Line is an immensely popular service which receives nearly 5000 calls every month, mainly from workers in Rajasthan and Gujarat. It receives and responds to calls of distress from workers who need urgent support. It also offers valuable information to workers on state schemes and programmes. So far, 2,00,352 labour disputes have been registered with Aajeevika, involving 1,17,838 workers. INR. 31.27 cr. has been recovered so far, majorly through mediation of wage disputes. Aajeevika also extends litigation support to workers or their kin in cases involving occupational diseases, workplace injuries or death. (Call 1800-1800-999 to register a case or seek information from the Rajasthan helpline).

Expanding this initiative at the national level, Aajeevika Bureau and the Working Peoples’ Charter (WPC) launched the National Helpline for workers. The event was conducted at the Press Club in Mumbai on September 18, 2021. The helpline, known as the India Labourline, has been operational since July 16, 2021 and is headquartered in Mumbai. In the last two months it has received 2497 calls and has provided mediation to 507 workers from states like Delhi, Rajasthan, Uttar Pradesh, Haryana, Punjab, Jammu and Kashmir, Madhya Pradesh, Odisha, Maharashtra, Karnataka, Kerala, Telangana and Tamil Nadu. Pending wages of INR.11,09,197 have been recovered through mediation in wage disputes. (Call 1800-833-9020 to register your dispute on the India Labourline. Services provided by the India Labourline are extended free of charge.).

Santosh Poonia and Divya Verma from Aajeevika Bureau opened the discussion with a presentation on the India Labourline. The presentation highlighted Aajeevika’s rationale for expansion of Labourline. The India Labourline responds to needs of migrant and informal workers, many of who feel underrepresented and neglected in policy and judicial processes. There is synergy between WPC’s national level network and Aajeevika Bureau’s experience in implementing a high impact model for resolving issues faced by informal sector workers. This initiative received support and funding from Azim Premji Philanthropic Initiatives, and seeks to democratise access to justice for the Indian working class.

After the insightful presentation, Justice Ajit Prakash Shah, former Chief Justice of the Delhi High Court, expressed grief at the present conditions and stated that “India is a state of many contradictions. Whereas we have millions of toiling masses who are struggling each day, the state refuses to provide them with adequate benefits.”

Indira Jaising, former Additional Solicitor General of India, mentioned that “the memory of the mass exodus that occurred last summer is still fresh in our minds. We must remember that workers build our country and we must make all attempts to ease their pain during this crisis.”

Baba Adhav, founding President of Hamal Panchayat and the Working Peoples’ Charter Network, said, “the WPC and the Aajeevika Bureau have made this attempt to establish parallel labour governance mechanisms when the current regime has failed.”

On behalf of Aajeevika Bureau and India LabourLine HQ,

Meena Menon, Rajiv Khandelwal and Chandan Kumar

Mumbai Centre – Amruta Paradkar

Bangalore Centre – Abhay Kumar and Nalini Shekhar

Hyderabad Centre – Varghese Theckanath

Delhi Centre – Ramendra Kumar

Lucknow Centre – Sandeep Khare

Head Office: 4th floor,Panchratna building, opp Chandelier Court, LRP apan Marg, Upper Worli, Worli, Mumbai- 400018

Phone number:022-40129763