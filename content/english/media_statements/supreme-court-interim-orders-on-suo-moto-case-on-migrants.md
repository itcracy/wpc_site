+++
bg_image = "/images/full.jpeg"
date = 2020-06-08T18:30:00Z
description = "##### The Supreme Court said to the central and state governments to complete transporting the remaining migrants headed home, adding to a roster of observations aimed at alleviating their misery."
image = "/images/full.jpeg"
tags = []
title = "SUPREME COURT INTERIM ORDERS ON SUO MOTO CASE ON MIGRANTS"
type = ""

+++
#### Key observations on orders of supreme court suo moto case on migrants;

The Supreme Court may have finally made a crucial intervention regarding the migrant workers crisis, even as it may be too little, too late. The Court recently directed the Centre and the state governments to ensure that the migrant workers reach their destination within 15 days. In addition to this, the key points of the apex court’s order can be summarised as below:

The Central Government/State Governments shall take further steps for identification of stranded migrant workers in their States who are willing to return back to their native place, and take all steps for their return journey transportation within 15 days. Ease of identification of the workers, decentralization of the registration process and mass-media dissemination of information regarding transportation should all be implemented for the same.

In addition to the States’/UTs’ demand of 171 Shramik trains, the Central Government shall provide more Shramik Trains within 24 hours of the request made.

Both the Central and State governments shall file affidavits giving details of all the employment schemes at the national and state-level that are available for migrant workers returned to their natives places.

State shall establish counseling centers at all block and district levels for informing and counseling migrant workers regarding the available employment schemes. These centers shall also provide assistance to those workers returned to their native place who want to go back to their erstwhile places of employment.

Details of all migrant workers who reached their native place shall be maintained village, block and district-wise to facilitate benefits of any government schemes applicable to migrant workers. States shall file affidavit giving details of the number of migrant workers who have returned to their state and further details of their previous employment, skills and available opportunity for them.

All States/UTs may consider withdrawing all the criminal complaints against the migrant workers against whom complaints were filed for violating the lockdown, made under Sec 51 of the Disaster Management Act or other related provisions.

While we appreciate that the above measures constitute a big relief, it is also crucial to highlight some of the pertinent questions raised by the order:

The process of identification and registration of the migrant workers has no system of accountability. While ordering for the simplification of registration process and allowing workers to register at nearby police stations or places of local administration, there was a great need to focus on police sensitization, and on active assistance of village/block/district level panchayat committees, councillors, MLAs, to help the workers. Registration forms must be in an accessible language medium, and State/UT governments should be urged to make full use of local media, billboard advertisements, radio and other outlets to publicise employment opportunities.

It is also important that care be taken about protecting the huge mass of data on migrant workers that exists after registration - what will be done to ensure their privacy, avoid enforced surveillance or exploitation of the data of lakhs of migrant workers. Many have already experienced similar difficulties with Aadhaar

The Supreme Court has taken for granted the Center’s submissions that Shramik trains provided food and water. Numerous reports have shown that the trains are grossly lacking in basic hygiene, and had absolutely no food available. There are reports suggesting several deaths on board the trains including 9 workers dying in a single day, these basic necessities being denied must be kept in mind while directing the Center to call in more trains.

The Court also has taken for-granted the Bihar, UP and other state government’s claims of paying each migrant worker journeying to their native place an amount of Rs 1000/-. As Rs1000 is not enough, case for Rs 7500 being paid to each worker as suggested by Advocate Indira Jaising is a more strong one. It is thus important that the Court look into interim relief and via encouraging more cash benefits.

There is no proof to suggest the Shramik trains have been free of cost. Multiple reports have shown that workers are charged for the train ticket prices. The Court must look into the same, and all the trains for the coming days must be made free-of-cost.

Every State/UT should have been directed to take initiatives for establishing mazdoor helplines/helpdesks and decentralization of the identification process. There must be some mechanism for accountability at each level from village to district to central.

The Court failed to ask for data or probe further into the States’ submissions that many workers had rejoined their prior places of employment. While providing statistics for the number of workers allegedly sent back home, the States provided no genuine assessment of the workers who stayed or rejoined. It is important to assess and document this, in order to be better prepared for the future.

The order makes no mention of the processes by which overall data and statistics of each State was arrived at - the claims regarding numbers of workers back to their native places need to be thoroughly vetted.

We would be tracking implementation of this order on ground and report back to court!

#### Please download detailed order here:

[Supreme Court Order on Migrants](https://drive.google.com/drive/u/3/folders/1pX9TswL4a410KwUTuYhn8qbdyM28X1_g)

#### Download a summary of orders here:

[English summary](https://drive.google.com/file/d/1XnhN5lV6WDA6CAo-3yfPiNzGNlPX7vvm/view?usp=sharing)

[HIndi Summary](https://workingpeoplescharter.in/documents/18/Hindi_-_Supreme_court_order_on_Migrants-%E0%A4%B9%E0%A4%A6.pdf)

[Telugu summary](https://workingpeoplescharter.in/documents/19/Telugu_SC_order_on_migrants_.pdf)

[Kannada Summary](https://workingpeoplescharter.in/documents/20/Kannada_-_Supreme_Court_Order_on_Migrants.pdf)

[Tamil Summary](https://workingpeoplescharter.in/documents/21/Supreme_Court_order_on_Migrants_-_in_Tamil.pdf)

[Marathi Summary](https://workingpeoplescharter.in/documents/24/Untitled_document.pdf)

[Central Govt affidavit -8th July 2020](https://drive.google.com/drive/u/3/folders/1pX9TswL4a410KwUTuYhn8qbdyM28X1_g)