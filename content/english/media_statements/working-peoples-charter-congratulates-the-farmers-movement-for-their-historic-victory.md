+++
bg_image = "/images/mcms.jpeg"
date = 2021-11-19T18:30:00Z
description = "WPC stands with their farmer brethren on this historic day that showcases the power of collective unity!"
image = "/images/mcms.jpeg"
tags = []
title = "Working Peoples’ Charter congratulates the farmers movement for their historic victory!"
type = ""

+++
**Working Peoples’ Charter congratulates the farmers movement for their historic victory!**

_19th November 2021_

The Working Peoples Charter (WPC) wholeheartedly congratulates the crores of farmers who have been struggling against the draconian pro-corporate and anti-people farm laws of the Central Government for the past one year. Today, on this historic day, the government has been forced to bow down to the farmers’ demands.

The farmers have sacrificed their lives and livelihoods in this nation-wide struggle. Their historic efforts have finally brought relief after battles against extreme, police brutality and dreadful corona wave that continues to ravage our nation. Nearly 700 farmers lost their lives, and it is the Central Government that shall bear the sole responsibility for these deaths.

The leaders of this struggle have reiterated that they will not rest until the farm laws are fully repealed through the due parliamentary process. They have also reminded us that the repeal of the laws in itself would not resolve the farm crisis. The farmers’ movement would continue to struggle for legal guarantees of MSP and procurement of their produce. Moreover the related issues of the Electricity Amendment Bill would also have to be addressed to assure the security of farmers’ livelihoods.

On this same note, the WPC calls upon its constituents, to build the widest coalition for strengthening the ongoing farmers’ movement for protecting the dignity and livelihoods of all working people of India!

Most India’s workers have tried to organize against the authoritarian diktats of the government. Millions of farmers and workers on the streets of India since the winter of 2017 have shown the government that this daylight robbery of farmers’ lands and rights will be resisted at all costs! All the government’s attacks on the farmers and workers’ organizations in this time has failed to break the historic farmer-worker unity

The Working People’s Charter is the voice of millions of informal workers with deep roots in the villages and farms of India. We call upon our constituents to stand shoulder to shoulder with our farmer brothers and sisters to inflict a resounding defeat to the flag-bearers of private profit and public loot!

**On behalf of the WPC Secretariat**

**Meena Menon (Working President), Raju Bhise (General Secretary), Chandan Kumar (Organising Secretary)**

For more information contact Pratik Kumar (Media Coordinator 9920486727)

[Press Release - 20.11.21](https://drive.google.com/file/d/1vkyY44Em4TdCV96o21djJLzUxzZ5KLoh/view?usp=sharing)