+++
bg_image = "/images/1-height-500.jpg"
date = 2021-03-22T18:30:00Z
description = "24 March marks the anniversary of India’s harsh nationwide COVID 19 lockdown when we witnessed an unparalleled impact on the country’s poor, particularly internal migrants who comprise a 140 million-strong workforce. In 2020, India saw the largest urban-rural exodus in its history, with millions of workers arduously trying to get to their homes on foot or remaining in hostile conditions on construction sites, factory floors, head loading markets, hotels and dhabas, and brick kilns, in shared rooms or makeshift homes in the open. They did this while battling decimated savings, homelessness, starvation, and the fear of contracting a virus about which little was known at the time."
image = "/images/1-height-500.jpg"
tags = []
title = "LEST WE FORGET: ONE YEAR AFTER THE LABOUR AND MIGRATION CRISIS"
type = ""

+++
##### A statement on the condition of India’s migrant workforce one year after the COVID-19 lockdowns

24 March marks the anniversary of India’s harsh nationwide COVID 19 lockdown when we witnessed an unparalleled impact on the country’s poor, particularly internal migrants who comprise a 140 million-strong workforce. **In 2020, India saw the largest urban-rural exodus in its history, with millions of workers arduously trying to get to their homes on foot or remaining in hostile conditions** on construction sites, factory floors, head loading markets, hotels and dhabas, and brick kilns, in shared rooms or makeshift homes in the open. They did this while battling decimated savings, homelessness, starvation, and the fear of contracting a virus about which little was known at the time. One of the most heart wrenching and shameful events that continue to haunt us was **the death of 16 workers near Aurangabad**, as they were en route home following a rail track.

The fact that India’s migrant workers had to bear the most severe brunt of the unplanned and ill-executed lockdowns needs no further belabouring. Neither does the fact that the **worker** crisis unfolded due to problems that have been **long-standing and unaddressed** across the country, including a steady devaluation of labour over decades, **inadequate housing and provisioning for urban poor groups, the lack of any policy framework on migration,** among others.

One year on, the vulnerabilities of migrant workers continue to persist in multiple ways. Those in rural areas struggle to survive amidst a lack of employment opportunities and a perpetually defunct rural welfare system. These systemic failures disproportionately affect Dalit and Adivasi workers, who often have only subsistence landholdings and comprise the majority of rural-urban migrant workers. In urban areas, workers are bearing the burden of economic recovery through poverty wages, normalised 12-hour workdays, and a continued lack of social security. At both the rural and urban ends of migration corridors, work and living conditions continue to be exploitative and undignified.

In response to the unprecedented nature of the labour and migration crises—as well as the attention, the issue received—the state has responded in limited ways. The governments of Chattisgarh and Telangana, for instance, formulated policies that are yet to be enforced. Other efforts, including the **One Nation, One Ration Card, while possibly introduced with good intent, have serious conceptual and implementation flaws, and vulnerable groups have not been able to access these entitlements yet.** While these efforts are a step in the right direction, it is deeply concerning that **India does not have a central, formidable policy or law to safeguard its migrant workforce despite the horrors of last year.**

In memory of the workers who lost their lives or were stranded in hostile cities and towns, we demand that the state **urgently conceptualise and implement an overarching, nationwide policy to help migrant workers across the country**. As many activists have reiterated, this includes **an urban employment guarantee, universal access to food, shelter, and related provisioning irrespective of migration corridor, strong grievance redressal mechanisms and legal aid, as well as better facilitation of social security and entitlements**. Importantly, the state must strictly regulate the industry, particularly principal employers who extract the most value from migrant workers, to **ensure workers are protected from wage theft and occupational accidents and harassment**. Furthermore, it is imperative for the government to carefully review/notify the recent labour codes; Code on Wages, 2019, the Industrial Relations Code, 2020, the Occupational Safety, Health and Working Conditions Code, 2020 and the Code on Social Security, 2020.

It is evident from recent developments that we are witnessing the second wave of the pandemic in India. Some state governments have already seen partial lockdowns and night curfews introduced a second time, were a **strict lockdown to commence again, its impact on informal workers is likely to be a lot more severe.** In the interest of the safety of India’s most vulnerable citizens, central and state governments must act swiftly and work together across party lines and differences.

The march of thousands of migrants should open the eyes of the powers that be. **Informal workers must to be guaranteed livelihood, coverage under ESIC, decent housing, including affordable rental housing for migrants.** They deserve **a life of dignity where people have a choice of occupation, opportunities to dream of a better life for their children, and the possibility to work in their home states or anywhere they may find a better livelihood.**

On the 23rd of March, 1931, Bhagat Singh, Sukhdev and Rajguru, was executed at 23 years of age. He fought for a free India based on justice for all. So today is an occasion to remember his sacrifice in the background of the sacrifices made by thousands of migrant’s workers who are still struggling for justice and equality in independent India.

**Download PDF:**

[WPC statement on one year of lockdown](https://drive.google.com/file/d/1cnkobwccJmO4LWD_SX9D02Zr6e6d041U/view?usp=sharing)

#### Image credit: Sanjana Nanodkar