+++
bg_image = "/images/73af161a-ee3c-4449-9b04-7379f111671f-height-500.jpg"
date = 2021-09-25T18:30:00Z
description = "The Central Government, in compliance with the orders of the Supreme Court of India, has initiated the process for launching the long-awaited national database portal on unorganised and migrant workers. This move is being welcomed and appreciated by the Working Peoples Charter (WPC), an all-India network of organizations working on issues related to unorganised, informal, and migrant workers. A time-bound implementation of the portal could prove to be effective in providing informal workers with much needed social security entitlements."
image = "/images/73af161a-ee3c-4449-9b04-7379f111671f-height-500.jpg"
tags = []
title = "E-SHRAM PORTAL, INDIA'S FIRST DATABASE FOR UNORGANIZED WORKERS HOLDS GREAT PROMISE"
type = ""

+++
The Central Government, in compliance with the orders of the Supreme Court of India, has initiated the process for launching the long-awaited national database portal on unorganised and migrant workers. This move is being welcomed and appreciated by the Working Peoples Charter (WPC), an all-India network of organizations working on issues related to unorganised, informal, and migrant workers. A time-bound implementation of the portal could prove to be effective in providing informal workers with much needed social security entitlements.

The e-Shram portal, set to launch today i.e., August 26, 2021, along with a national toll-free number 14434, will assist and address the queries of workers seeking registration. Following the launch, the workers can register on the portal using their Aadhaar and bank account details and will then be issued an e-Shram card containing a 12-digit unique number.

While the WPC celebrates this positive step, it is important to acknowledge the heavy price paid for this by the workers. Thousands of migrant workers lost their lives after they were abandoned by the Central and State Governments as soon as Covid-19 hit India. By imposing the strictest lockdown in the world, the States forced a mass exodus of workers. The unpreparedness of the Ministry of Labour and Employment, to deal with the Covid-19 migrant crisis, was exposed a day before the lockdown when they issued a statement saying that “no migrant worker register was maintained”. It is imperative to remember the vast constituency of working families who continue to toil each day without any governmental support and only time will tell if this database proves to be effective in delivering justice for the informal workers and restoring their dignity.

Hence, WPC would like to express certain concerns and provide suggestions made by worker groups, and labour academia with extensive experience on labour issues.

**Concerns:**

1. The portal has limited access as workers who do not have an Aadhaar card will not be able to register.
2. At present, social security schemes and entitlements are not part of the process. There are proposals to provide access to some schemes, such as accident insurance (amounting to INR 2 lakhs) and benefits arising under the Pradhan Mantri Suraksha Bima Yojana (PMSBY), but details of other schemes remain missing.
3. The database only asks for the father’s name and not the mother’s name. This might create gender exclusion.
4. The current system does not include the workers who are already registered under different boards or how it will migrate this existing data into a national database.
5. This scheme only operates for individual workers who are getting registered. There is no provision to include details of families or dependents of the workers.
6. Elderly workers, above the age of 60, are being arbitrarily excluded. It is pertinent to note that majority of informal workers are not covered under any pension scheme.
7. Authentication of workers for the registration is only done by the Shram Suvidha Kendra, which itself is problematic as it might lead to exclusion while handling the database of over 43 crore workers.
8. The whole registration system does not take into consideration the ‘Digital Divide’ which is creating barriers for workers to not only access but also understand it. Workers have not been receiving any communication/notification/link after submission or UAN No. even after getting the card.

**Proposed suggestions to govern the process of registration of workers:**

**Inclusion of all workers:**

A Standard Operating Procedure (SOP) has been proposed (attached below as ‘Annexure’), which, if implemented, can help in the inclusion of most of the workers from different sectors, Therefore, workers’ organizations should be involved in facilitating the registration process to avoid exclusion along with awareness and training programmes.

**Registration process:**

1. Other government-issued identity cards such as a voter ID card should also be included as valid proof as an alternative to an Aadhaar card.
2. Self-declared information shared by workers must be treated as true unless proven incorrect. The burden of proof for the veracity of information should lie with the States and not on the individual worker. A worker wishing to register himself/herself should not be required to give any proof or any other personal details except those that may be necessary for contacting him/her. Recognizing that, despite best efforts, the modes of registration can be corrupted or blocked, multiple modes (online and offline) and routes (multiple spatial access points) must be provided by the States to make it progressively difficult to inhibit attempts made by workers to register in a free and fair manner. Therefore, workers organisations should be made a part of the registration and authentication (fixing any errors) system.
3. Once the workers are registered in the system, they should get a link or a text with the UAN number with easy to download pdf to get a printout of the card.
4. Workers who are already registered on existing government portals/lists should not be asked to re-register themselves. Their inclusion in the national database portal should be automatic. In such cases of auto-enrolment, the worker’s unique registration number should be communicated to the worker by SMS on the given phone number and the worker’s registration card should be sent by post to the address in the database. However, if a person who is registered on any of the portals or lists appear at the registration desk, s/he should not be denied registration. An acknowledgement receipt with the ID, list from which the name has been pulled, and status (active/inactive / pending for renewal etc.) should be given along with the physical copy of the worker’s registration ID card.
5. It should be the responsibility of the State/Central Government to take pro-active steps such as organizing registration camps, door-step registration services, etc. to reach the worker and facilitate him/her through the process of registration and other incidental activities, instead of relying on the worker to contact the State/Central Government.

**Information and benefits of registration:**

1. A formal tripartite advisory committee to assist/support the State/Central Government should be set up which could also help in designing sector-specific tailor-made schemes to ensure workers’ access to existing social security entitlements.
2. There is a constant demand by worker organisations to constitute sectoral boards for social security and the welfare of all the unorganized workers of the country under the Social Security Code 2020 (SS Code). In line with this, the Central/State Government should also notify the rules immediately under this code.
3. The Central/State Government must also put in place mechanisms through which the workers are made fully aware of all the information regarding the registration process, such as knowing what registering to this database means, what benefits it will give them, and who will have access to the information to name a few.
4. Worker’s organisations should be roped in for implementing registration and awareness drives.
5. All workers should be able to register anywhere, get information anywhere, access a centre anywhere. Benefits and coverage that follow from the ID must be accessible anywhere in India and should be designed that way.

The implementation of the national database portal will benefit in giving an identity to the worker. However, it is important to incorporate plans to form boards and access social security entitlements for the well-being of unorganised and migrant workers. Therefore, we believe that the government will look into the above-mentioned concerns and consider the suggestions provided above and in the annexed SOP.

**WPC Secretariat**

**For more details, please call**

Chandan Kumar - +91 9717891696 Pratik Kumar- +91 9920486727

##### **Please click here to download the document in Hindi and English:**

[WPC Press Release - E-Shram Portal](https://drive.google.com/file/d/1BVbB68wbzzaNCr9_zRbJ-1gJjNvVbaoA/view?usp=sharing)

[WPC Press Release in Hindi - E-Shram Portal](https://drive.google.com/file/d/1cAbRvVKL-rHNdeBmW40Ne1gPA1uSAIx3/view?usp=sharing)

Picture Credit: Deepak Paradkar, India LabourLine and Aajeevika Bureau, Mumbai