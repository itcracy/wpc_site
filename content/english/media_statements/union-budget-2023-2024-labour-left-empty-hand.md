+++
bg_image = "/images/w.jpg"
date = 2023-02-07T10:30:00Z
description = "The Working Peoples Coalition (WPC) notes with concern the budget of the Union government when the workers in the country are still reeling under the effects of the pandemic and the economic crises. Workforce participation has not been restored to pre-pandemic levels, with a significant decline in women's participation; the massive informal sector workers are suffering from unemployment and inflation; and vulnerability and inequality are on the rise. Unfortunately, the administration continues to ignore reality, and the first chapter of the Finance Ministry's current Economic Survey is titled \"Recovery Complete.\""
image = "/images/images.jpg"
tags = ["labour_axis"]
title = "Union Budget 2023- 2024: Labour left empty handed"

+++
The Working Peoples Coalition (WPC) notes with concern the budget of the Union government when the workers in the country are still reeling under the effects of the pandemic and the economic crises. Workforce participation has not been restored to pre-pandemic levels, with a significant decline in women's participation; the massive informal sector workers are suffering from unemployment and inflation; and vulnerability and inequality are on the rise. Unfortunately, the administration continues to ignore reality, and the first chapter of the Finance Ministry's current Economic Survey is titled "Recovery Complete."

The budget has little to offer in terms of job creation and social security. **The Union Budget has drastically slashed MGNREGA funds for 2023-24 to Rs 60,000 crore**. This year's budget for the flagship rural employment programme is the smallest in six years. Furthermore, Rs 10,000-15,000 will be spent on wage arrears, leaving only Rs 45,000 for job development and rural asset generation. Ministry of Rural Development has subsequently clarified that the **funds released for MGNREGA allocations have always been higher than the budget estimate,** we believe that such a model has led to increased delays in fund release from the Centre, causing delays in wage payments to households who solely depend on the scheme for their livelihoods. **Reduced MGNREGA financing would essentially imply less rural needs and livelihoods, driving millions to migrate to cities in pursuit of insecure jobs and an uncertain future. We strongly urge that an Urban Employment Guarantee Programme (UEGP) (similar to MGNREGA) has to be introduced for the urban poor**. Such a programme should be designed to cater to the employment needs of various vulnerable segments among the urban poor like women, pensioners, and more importantly, migrant workers. **Evidence from MGNREGA, and several state-level UEG schemes, shows how such a programme has immense potential for community asset creation and urban infrastructure as well, apart from sustainable job creation.**

**The agricultural sector has also been disregarded in the budget, which will exacerbate the rural crisis.**

**Apart from the credit-linked subsidy scheme, there is nothing in the budget for Micro, Small, and Medium Enterprises (MSMEs).** While the emphasis should be on micro and informal sector industries, which account for around 98-99 per cent of all small units and offer significant employment, lending and other support to these sectors has been shrinking in recent years.

In truth, the government's argument that **significant investment will create jobs is highly misleading**. **Employment generation through capex creation may result in some skill-oriented positions, but the expansion of jobs at the bottom of the pyramid will be minimal.**

It is obvious that the growing rural crisis will drive more and more workers to urban spaces which have limited capacity to accommodate this migrant workforce in terms of livelihood and other infrastructure including habitat, health, transport, etc. **The creation of the Urban Infrastructure Development Fund for lending shortfalls in the budget is much appreciated. Having said that 10,000 cr is a very small amount given a huge repository of priority sectors in tier 1 cities and the need for heavy investment for tier 2 and 3 cities**. Municipal bonds and efficient use of land as resources have been identified as prominent strategies to raise funds for municipal corporations. **Experience shows that only attractive and large municipalities benefit from municipal bonds and optimisation of land as a resource in the past has led to massive evictions** and only minuscule rehabilitation of the ones evicted.

The announcement of the major allocation of funds was towards PMAY (Gramin), with an amount of Rs 54,487 crore for the current financial year 2023-24. This has been the highest-ever allocation since the inception of the PMAY (G) scheme in 2016. **This has left PMAY(Urban) with the allocation of Rs 25103 crore from Rs 28708 crore (2022-2023).** It is now expected that these **funds will now be utilized to build housing under the CLSS vertical of PMAY (U) for the EWS and LIG category and therefore slashing the budget for the urban version of the scheme means that the dreams of owning a house for the citizens of Slums/Bastis will be pushed back. Rental housing for migrant workers does not even find mentioned anywhere. Clearly, the government and media have forgotten how millions of workers had to trudge back to their villages when the lockdown forced them out of their meagre habitats at their city of work.**

A lot of emphases is laid on **regional connectivity and transit-oriented development**. Most inexpensive and sustainable is the road connectivity, however, the **majority of proposals in the budget are for the construction of fifty additional airports, heliports, water aerodromes and advance lending grounds. This will definitely not benefit the informal working population**.

The budget mentions that youth empowerment is one of the corner points in the _Amrit kaal._ The budget mentions it will launch Pradhan Mantri Kaushal Vikas Yojna 4.0, which will skill lakhs of youth within three years. While these steps are appreciated, the budget throws no realistic light on either the current status of job opportunities across different industries nor the much-required solutions for the lakhs of unemployed and under-employed youth. Just when the manufacturing industries are beginning to recover, a tokenistic mention of linking formal education under NEP, with skilling youth in the industries, does not sound very optimistic. On the other hand, while the National Education Policy’s focus on Skilling as one of the focal cornerstones for empowering the youth is itself a very narrow view of formal education, the budget seems to be devoid of any realistic analysis of the needs of manufacturing and service sector industries and the kinds of job opportunities these industries are creating post-pandemic. When the labour is being pushed into contractual, informal and precarious kinds of work, that lack provisioning of even minimum wages and social security, how can we expect that the benefits of this skilling programme will reach these workers who are invisible from all forms of formalisation**? Experience from the past shows that the apprenticeship scheme that has been highlighted in the budget is actually just a means of providing the industry with cheap labour that can be employed in unfair conditions and discarded at will.**

**This dire situation necessitates even stronger opposition to cuts in social security and social protection. Subsidies to the wealthy are to be discontinued immediately. To reverse rising inequality and build a more equitable and just society, we must fight for an economic policy that ensures a job for every worker, a fair wage for every job, universal access to health care, free education for all, and a roof over everyone's head.**

***

Meena Menon, Working President

Raju Bhise, General Secretary

***

For further information pls contact –

Shweta Damle (9869240816)

Divya Varma ( 9606359333)

***

###### **Download the full statement** [**here**](https://drive.google.com/file/d/1Pd-o8p0Rsr8ViYTI7NR7FSR9s-U6Mz5o/view?usp=sharing)