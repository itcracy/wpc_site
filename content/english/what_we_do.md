---
title: About Us
description: The WPC is a loose network of organizations, networks, and federations
  working on labour and labour related issues, with a focus on informal, low paid
  and migrant workers.
bg_image: "/images/w.jpg"
menu:
  main:
    URL: what_we_do
    weight: 2

---
### WHAT IS THE WORKING PEOPLES Charter (WPC) NETWORK, INDIA?

The Working Peoples Charter (WPC) network is a coalition of organizations working on issues related to informal labour in particular and labour in general. It is an independent entity not affiliated with any organization, federation or political party. The WPC is open to all organizations who are in broad agreement with the Working Peoples Charter and are working with informal labour, including in organizing, support, research, training, skilling etc., irrespective of their affiliation or sector.

#### **The Charter:**

Initially, the charter was intended as a document that would serve as a manifesto for constituent organizations to be used during the 2014 Lok Sabha Elections. Several demands converged, relating to social security, conditions at work, the security of tenure, the right to organize and minimum wages, with several organizations rallying behind the charter document and making parliamentarians accountable at their local level. Following the elections, with the BJP--NDA win, a broader alliance could be formed around the charter document to bring together a wider variety of organizations and workers from across the country to rally behind the demands of the Working Peoples Charter. A process began to emerge and eight-core areas emerged around which the constituent groups converged. They included: Minimum Wages, Social Security, Labour Administration, Labour Law Reform, Right to Work, Workers Housing, Bonded Labour and Migrant Workers.

#### **Why WPC:**

Whether it is the burden of high growth or the high cost of an economic crisis, the working class always has to sacrifice in terms of jobs, wages, social security and personal safety. The need for coming together, analyzing and understanding the situation and developing strategies for survival and struggle is more critical than ever before, with widespread informalisation of labour across all sectors and geographies resulting in the weakening of the labour movement. The WPC network:

* Magnifies the voices of people who are mostly unseen and unheard;
* Makes it possible to take up common issues of various sectors of the workers and represent these issues before the authorities; and
* Through its relatively open nature and organized around a workers’ charter, the charter makes this network more diverse so that all kinds of organizations working in the informal sector can come together.

#### **What the WPC does:**

##### The activities of the WPC include the following:

1. Formulating the key common demands of informal workers;
2. Building a wide network, consisting of trade unions, organizations working with informal labour, lawyers, academicians, activists, and researchers;
3. Advocating for the implementation of key demands in labour policy and making it successful;
4. Negotiating with government ministries for these demands, including wages and benefits, for social protection;
5. Building a strong, well-informed, enlightened leadership from the grassroots organizations;
6. Providing help for mediation and legal aid to workers;
7. Developing responses to labour policy from the point of view of the informal workers, with a special focus on issues of women workers;
8. Understanding and building the relationship between workers, work, livelihood and the city; and
9. Engaging in relief work during times of disaster or crisis.

#### **India Labourline**: **Helpline for Mediation and Legal Aid:**

With the help of Aajeevika Bureau, WPC has set up an all India national Mediation and Legal Aid Centre with links in different states to help workers, especially migrant workers, to ask for help with the issues they may be facing at their places of work, and to provide information that can help them access their entitlements. The centre is based in Mumbai and paralegal staff are available in five states, to begin with, namely Delhi, UP, Karnataka, Telangana, and Maharashtra. By 2022, the centre will expand to more states.

#### **Labour Axis - Knowledge and Research:**

WPC has groups of experts to help with training, education and policy development, in order to generate briefing papers and statements. WPC works with Labour Axis, a knowledge network consisting of researchers working on labour related issues who work with the network to help in formulating positions and informing campaigns. The WPC’s work on labour also includes an understanding and engagement with citizenship, sustainable development and the city. WPC works in a social and political context and the place of labour within it. It sees labour in the context of an exploitative system based on profit above all else. It sees class in intersectionality with gender, caste and race. It is committed to building a democratic, sustainable and climate-just world.

#### **Lawyers for Labour:**

WPC has taken up an initiative to bring together lawyers working on labour issues, who can help WPC constituencies and work of the India Labourline, Labour Axis and campaign groups. The network will also reach out to para-legal workers, notaries etc.

#### **Sanchaar - Network of Interpreters:**

Language is a major obstacle for an organization with national level networks of grassroots organisations. Sanchaar is being set up to facilitate communication between activists in various states who are divided by barriers of language.

#### **What makes WPC unique?**

##### WPC is unique in terms of its understanding, methodology and reach.

1. Despite differing political and social backgrounds, the charter is able to come together without having to agree with each other on all aspects of policy or ideology. There is a fair level of good team spirit and commitment as it is a non-sectarian space.
2. Despite initial hostility from the central trade unions, this process of informal sector organizations coming together has attracted support from the central trade unions as well. They are also coming forward to be part of WPC events, discussions and actions. The network is diverse in its constituencies, regions, issues, perspectives, and histories, but they are singularly committed to the cause of making a better life for informal workers.
3. The WPC has a ‘wraparound’ model: meaning that it tries to provide all the aspects needed to strengthen the workers' organizations working in the field.
4. Since its inception WPC recognizes and works actively toward the following:
   1. The need for social change;
   2. Knowledge to imagine that change concretely;
   3. Strategy for such a change, including engagement with civil society as well as government;
   4. Educating and empowering the local leadership of the organizations working in the section; and
   5. Emphasis on strong research and knowledge component to enable the network and the members to formulate demands and design solutions in consultation with governments, employers, legislators and academics.

#### **Where does WPC work?**

WPC includes all sectors of informal labour: construction, domestic, brick kilns, care work, self-employed, farm labour, forest workers, health workers, gig workers, sweatshop labour, contract workers, migrant workers and many more. The network has a grassroots presence in more than thirteen states of India, although the consolidation of the presence is yet to be completed. These states include - Andhra Pradesh, Assam, Delhi, Jharkhand, Karnataka, Maharashtra, Pondicherry, Rajasthan, Uttar Pradesh, Tamil Nadu, Telangana, West Bengal, and some North-Eastern states. The network is still in its growth phase and is covering more states slowly.

#### **Structure**

1. The structure is as follows:
   1. Annual Conference of all member organizations;
   2. National Coordination Committee (NCC) and National Council; and
   3. State-level chapters of members organizations and a wider constituency of individuals and supporters
2. The WPC Annual National Conference elects -National Council
3. There is no office bearers as per the newly adopted resolution. The NCC is a collective leadership of WPC, who will implement the decision taken up by national council. The National Council is apex decision making body of WPC.

#### **Membership drive:**

WPC is canvassing membership and establishing state-level WPC coordination committees in every state. Organisations are encouraged to email or call for details. Member organisations in the various states are requested to actively reach out to organizations in various sectors throughout the state in order to reach as many as possible.

##### The conditions for the membership are:

1. It must be a union, NGO or research group – an organization of any kind that is engaged primarily in work that helps to access rights or better the conditions for labour;
2. There is a broad agreement with the Charter of the Working Peoples Charter;
3. Membership should be approved by at least two members in the state (or one of the states and the head office) where the organization is based; and
4. There is a broad commitment not only to labour rights but also to rejecting misogyny, casteism and all kinds of prejudice and injustice.

#### **Thematic Areas:**

WPC largely works for informal labour, i.e., people dependent on the informal economy for a livelihood.

##### Besides this, we will also work on:

1. Issues and demands of workers in the newer sectors: eg. platform economy
2. Access to judicial recourse for all workers
3. Unsafe migration and trafficking
4. Access to and protection of natural resources
5. Focus on gender: perspectives, policy and issues
6. Climate justice and resilience as a labour issue
7. Caste-based and social identities form the basis for many kinds of discrimination related to employment

We firmly believe that the time has come to join together across the country through this wide network to take up common issues of labour, show solidarity with each other, draw on the strengths and experiences of each organization or network and bring critically needed change in the lives of workers in our country.

#### Please join the network!

###### [Click here to contact us for more information or clarify any questions.](https://workingpeoplescharter.in/contact/)