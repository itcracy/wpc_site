---
title: India Labourline
description: "Call our number: \n\n# 1-800-833-9020"
bg_image: "/images/w.jpg"
related_tag: india_labour_line
image: "/images/india-labourline-logo.jpg"
facts:
  enable: true
  fact_item:
  - name: 'Workers Reached '
    count: 115413
  - name: 'Cases Registered '
    count: 4911
  - name: 'Cases Resolved '
    count: 1166
  - name: Money Recovered (INR)
    count: 30000000

---
##### With the help of Aajeevika Bureau, WPC has set up the India Labourline to provide legal aid and mediation services to workers, specifically migrant workers. The Labourline has offices in five states and is open to providing free help to workers in any state.

While the COVID pandemic is a huge crisis, it has vividly shown the urgency to create a legal and mediation framework which will address the issues and gaps in providing legal aid and mediation for the problems faced at the worksite. The pandemic emphasized the need for strengthening and reforming the Labour Department: plagued by inefficient governance and administration and a lack of human and financial resources. The lockdown affected the migrants and unorganised workers most, because of the lack of alternatives to fend for themselves, and no means for accessing information regarding schemes or entitlements as required.

#### India Labourline is a direct response to the need for support for migrant workers and all those working in the informal sector in India.

Informal work means precarious work, involving irregular and insecure work in unsafe workplaces, for long hours, low wages and no security. In addition to this is the high prevalence of work-related frauds such as wage theft, bondage, physical and verbal abuse, and non-payment of compensation following accidents in hazardous workplaces. There are deep-rooted vulnerabilities in terms of access to basic necessities in times of crisis as was seen during the pandemic and the lockdowns. The helpline, which started operation on July 16th, tries to address these and other issues by providing information, advice, mediation, and legal aid as needed.

India Labourline is headquartered in Mumbai. The tele counsellors are trained to provide the first level of counselling/intervention over the phone to workers before handing it over to the concerned State Facilitation Centres, which are responsible for undertaking field-level interventions in legal disputes that are registered with the Helpline. State centres are functional in the following states at present: Maharashtra, Delhi, Uttar Pradesh, Karnataka, and Telangana. The helpline also helps workers by connecting them to appropriate stakeholders, such as the police, labour departments etc. In cases where all mediation or referral have failed, the workers are given help to approach the courts.

The helpline has been operational since July 16, 2021, and has its headquarters in Mumbai. In the last six months, the helpline has received thousands of calls from workers and has provided mediation help to workers in distress from states like Delhi, Rajasthan, Uttar Pradesh, Haryana, Punjab, Jammu Kashmir, Madhya Pradesh, Odisha, Maharashtra, Karnataka, Kerala, Telangana and Tamil Nadu. It was started in response to the urgency to create a framework to address the issues and bridge the gap in providing legal aid and mediation services for the problems faced by workers at the worksite. The lack of such resources was clearly evidenced by the plight suffered by the workers during the Covid-19 pandemic. The pandemic also emphasized the need for strengthening and reforming the Labour Department which is currently plagued by inefficient governance and administration and an absence of human and financial resources.

**Media Coverage:**

[How A Labour Helpline Is Helping Informal Workers Recover Wages](https://www.indiaspend.com/governance/how-a-labour-helpline-is-helping-informal-workers-recover-wages-access-entitlements-828966), By [Shreehari Paliath](https://www.indiaspend.com/author/shreehari), IndiaSpend