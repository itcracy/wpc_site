---
title: Our Team
bg_image: "/images/b5e70151-10c7-3c2b-faeb-6b26c7d98248.jpg"
description: |-
  The WPC network is a collective rather than a federation and will continue in the present form since this gives the maximum space for democratic functioning and debate, and maintains the independence of decision making of the separate organisations. The WPC is not a registered organization at the present time. However, it needs a structure in order to function as an effective organization. In order to fulfil its objectives, to be effective in its mission, the WPC has the following structure:

  * National Conference, (assisted by National Sub-Committees and Thematic Groups )
  * Executive Committee
  * Working Committee (including office bearers)
  * National Secretariat
  * State chapters

---
