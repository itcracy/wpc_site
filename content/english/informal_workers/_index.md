---
title: Informal Workers
bg_image: "/images/w.jpg"
description: The Indian workforce is characterized by a small formal / organized sector
  of the economy and a huge informal / unorganized sector, both in agriculture and
  non-agriculture.
facts:
  enable: false
  fact_item: []
team:
  enable: false
menu:
  main:
    URL: informal_workers
    weight: 5
    parent: home

---
#### An informal worker in India is a woman, a man, a minor, a dalit, an adivasi, a religious minority, a member of a sexual or gender minority, a member of a nomadic or de-notified tribe, subsisting through profound insecurities of employment, income, health, food, housing, child support, and many other things that constitute a decent life with dignity.

An informal worker is a labourer-bound to a landlord, a contractor, a usurer – for generations. An informal worker is footloose, a migrant, pushed out of their homes and unwelcome at their destination. An informal worker is not a worker in the eyes of the law. An informal worker is everyone without labour rights. 9 out of every 10 workers in India are informal and largely unorganized into collectives.

**And when a calamity like COVID-19 hits the world, the country, the town, or the village, it is the informal workers who hold up the society and the economy – yet unpaid, yet unseen and yet unheard!**

#### **Registration with Recognition**

1. We welcome the union government’s initiative of registering informal workers, especially on the basis of self-declaration by workers, in the e-Shram Portal, provided no worker is deprived of existing entitlements consequentially. Crores of workers have already expressed their hope of ultimately gaining recognition of their work and entitlements. However, practical challenges have emerged in the registration process that instil fear of unfair exclusion among large sections of workers. The government must immediately allay such fears through broad-based consultations and awareness campaigns, wherein representative organizations of workers can become active facilitators of the registration process.
2. We demand that the benefits to be disbursed through the e-Shram portal for unorganized workers be portable and at par with the benefits available to formal sector workers in terms of their quantum and quality. For instance, the pension amount under the Pradhan Mantri Shram Yogi Maan-Dhan (PM-SYM) Pension Yojana and the National Pension Scheme for shopkeepers, traders, and the self-employed persons (NPS-Traders), should be based on average salary/income during the preceding 12 months from the date of exit and total years of employment, as in the Employees' Pension Scheme 1995 (EPS).
3. We remind ourselves that the registration of unorganized workers is the first step towards, not only achieving social protection, but also recognizing long overdue labour rights as envisioned in our Constitution (Articles 37, 38, 39-A, 41, 42, 43, 45 and 47) and the International Labour Organization’s (ILO) Declaration on Fundamental Principles and Rights at Work 1998. All the unorganized workers of India must be entitled to freedom of association and the effective recognition of the right to collective bargaining, the elimination of forced or compulsory labour, the abolition of child labour and the elimination of discrimination in respect of employment and occupation.