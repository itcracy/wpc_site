---
title: Partners and Allies
description: 'Solidarity to Migrant Workers '
bg_image: "/images/w.jpg"
type: theme
facts:
  enable: true
  fact_item:
  - name: Number of State Chapters
    count: 8
  - name: Number of Organisations/Unions
    count: 151
  - name: Number of states with grassroot presence
    count: 18
success_story:
  enable: false
  bg_image: ''
  title: ''
  content: ''
  video_link: ''
menu:
  main:
    URL: partners_and_allies
    weight: 9
    parent: home

---
#### Working Peoples' Coalition:

A network of more than 151 provincial and local informal workers organisations was founded in 2013 to fulfil this vacuum. The Charter believes in the capacity building of network members.  This will help the members wage struggles at the provincial and national levels.  The capacity building includes an understanding of laws pertaining to informal workers, including the proposed bills on wages, industrial relations, occupational health and safety and social security in the legislature. Currently, no such platform exists in India voicing collective concerns of informal workers. This positioning of being the first and the only network at the national level gives it a competitive advantage.

The network has a grassroots presence in the place in more than eighteen states of India- Andhra Pradesh, Assam, Bihar, Delhi, Karnataka, Maharashtra, Madhya Pradesh, Odisha, Punjab, Rajasthan, Telangana,  Uttar Pradesh, Tamil Nadu, Punjab, Haryana, Uttarakhand, Assam and West Bengal. It has vast experience in negotiating with municipal, state and national governments with the higher participation of both grassroots activists and workers.  In this context, the Working Peoples’ Charter envisions that every worker has a right to work and quality employment.

[The WPC Charter](https://docs.google.com/document/d/11mzXMYe14TNTZe-Z2R2UpPKkbFyMADty/edit)
